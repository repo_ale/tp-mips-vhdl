library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- The memories are not incorporated into the processor. They are
-- separate entities that interact with the processor, one for
-- instructions and the another for data.
entity processor is port(
	--clock signal
	Clk			: in	std_logic;
	--reset order
	reset		: in	std_logic;

	--target address in the instruction memory
	I_Addr		: out	std_logic_vector(31 downto 0);
	--reading control signal for instruction memory
	I_RdStb		: out	std_logic;
	--write control signal for instruction memory
	I_WrStb		: out	std_logic;
	--data sent to instruction memory
	I_DataOut	: out	std_logic_vector(31 downto 0);
	--instruction received from instruction memory
	I_DataIn	: in	std_logic_vector(31 downto 0);

	--target address in the data memory
	D_Addr		: out	std_logic_vector(31 downto 0);
	--reading control signal for data memory
	D_RdStb		: out	std_logic;
	--write control signal for data memory
	D_WrStb		: out	std_logic;
	--data sent to data memory
	D_DataOut	: out	std_logic_vector(31 downto 0);
	--data received from data memory
	D_DataIn	: in 	std_logic_vector(31 downto 0)
);
end processor;

architecture processor_arq of processor is

-- Carries out add, sub, and, or operations requested by instructions,
-- as well as calculating branch conditions.
component ALU port(
	input_a		: in	std_logic_vector(31 downto 0);
	input_b		: in	std_logic_vector(31 downto 0);
	ALUctrl		: in	std_logic_vector(2 downto 0);
	zero		: out	std_logic;
	result		: out	std_logic_vector(31 downto 0)
);
end component;

-- Temporary registers for storing operands, results and loaded words
component Registers port(
	Clk			: in std_logic;
	reset		: in std_logic;
	w_control	: in std_logic;
	reg1_addr	: in std_logic_vector(4 downto 0);
	reg2_addr	: in std_logic_vector(4 downto 0);
	reg_write	: in std_logic_vector(4 downto 0);
	data_write	: in std_logic_vector(31 downto 0);
	data1_read	: out std_logic_vector(31 downto 0);
	data2_read	: out std_logic_vector(31 downto 0)
);
end component;

--Signals will be "repeated", in order to implement the pipeline.
--For example, EX_ALUop won't take ID_ALUop's current value until
--Clk goes high. At the same time, ID_ALUop's value will update,
--behaving as an intermediate register.


-- IF STAGE SIGNALS
--current instruction address
signal IF_PC		: std_logic_vector(31 downto 0);
--current instruction address+4
signal IF_PC_4		: std_logic_vector(31 downto 0);
--control signal for mux picking next instruction
signal IF_PCSrc		: std_logic_vector(1 downto 0);
--next instruction address
signal IF_next_PC	: std_logic_vector(31 downto 0);
--IF/ID flush signal
signal IF_flush		: std_logic;
--PC + 4 before branch
signal old_PC_4		: std_logic_vector(31 downto 0);

-- ID STAGE SIGNALS
--instruction to decode
signal ID_instruction		: std_logic_vector(31 downto 0);
--instruction address+4
signal ID_PC_4				: std_logic_vector(31 downto 0);
--data at register rs
signal ID_data1_read		: std_logic_vector(31 downto 0);
--data at register rt
signal ID_data2_read		: std_logic_vector(31 downto 0);
--write control signal for registers
signal ID_reg_write_control	: std_logic;
--rs register address
signal ID_rs				: std_logic_vector(4 downto 0);
--rd register address
signal ID_rd				: std_logic_vector(4 downto 0);
--rt register address, for loading operations
signal ID_rt				: std_logic_vector(4 downto 0);
--alu operation
signal ID_ALUop				: std_logic_vector(1 downto 0);
--ALU source mux control signal
signal ID_ALUsrc			: std_logic;
--signal which controls the mux that chooses the register to write in
signal ID_RegDst			: std_logic;
--enables branch condition
signal ID_branch			: std_logic_vector(1 downto 0);
--data memory write control signal
signal ID_mem_write			: std_logic;
--data memory read control signal
signal ID_mem_read			: std_logic;
--signal which controls the mux that chooses what data to write into
--the registers
signal ID_MemToReg			: std_logic;
--extended immediate value for branching address
signal ID_immediate			: std_logic_vector(31 downto 0);
--ID/EX flush signal
signal ID_flush				: std_logic;


-- EX STAGE SIGNALS
--instruction address+4
signal EX_PC_4				: std_logic_vector(31 downto 0);
--data at register rs
signal EX_data1_read		: std_logic_vector(31 downto 0);
--data at register rt
signal EX_data2_read		: std_logic_vector(31 downto 0);
--write control signal for registers
signal EX_reg_write_control	: std_logic;
--register address to write
signal EX_reg_write_addr	: std_logic_vector(4 downto 0);
--rs registe address
signal EX_rs				: std_logic_vector(4 downto 0);
--rd register address
signal EX_rd				: std_logic_vector(4 downto 0);
--rt register address, for loading operations
signal EX_rt				: std_logic_vector(4 downto 0);
--alu operation
signal EX_ALUop				: std_logic_vector(1 downto 0);
--ALU b input source mux control signal
signal EX_ALUSrc			: std_logic;
--ALU a input
signal EX_ALU_A_input		: std_logic_vector(31 downto 0);
--possible ALU b input
signal EX_p_ALU_B_input		: std_logic_vector(31 downto 0);
--ALU b input
signal EX_ALU_B_input		: std_logic_vector(31 downto 0);
--ALU a forwarding mux control signal
signal EX_ALUfwrdA			: std_logic_vector(1 downto 0);
--ALU b forwarding mux control signal
signal EX_ALUfwrdB			: std_logic_vector(1 downto 0);
--signal which controls the mux that chooses the register to write in
signal EX_RegDst			: std_logic;
--enables branch condition
signal EX_branch			: std_logic_vector(1 downto 0);
--data memory write control signal
signal EX_mem_write			: std_logic;
--data memory read control signal
signal EX_mem_read			: std_logic;
--signal which controls the mux that chooses what data to write into
--the registers
signal EX_MemToReg			: std_logic;
--extended immediate value for branching address
signal EX_immediate			: std_logic_vector(31 downto 0);
--branch address
signal EX_branch_addr		: std_logic_vector(31 downto 0);
--ALU result
signal EX_ALUresult			: std_logic_vector(31 downto 0);
--alu contrl	¿qué va aquí?
signal EX_ALUctrl				: std_logic_vector(2 downto 0);
--signal which indicates if a zero was obtained when evaluating
--conditions
signal EX_zero				: std_logic;
--EX/MEM flush signal
signal EX_flush				: std_logic;

-- MEM STAGE SIGNALS
--signal which indicates if a zero was obtained when evaluating
--conditions
signal MEM_zero				: std_logic;
--data memory write control signal
signal MEM_mem_write		: std_logic;
--data memory read control signal
signal MEM_mem_read			: std_logic;
--data read from memory
signal MEM_mem_data			: std_logic_vector(31 downto 0);
--ALU result
signal MEM_ALUresult		: std_logic_vector(31 downto 0);
--data at register rt
signal MEM_data2_read		: std_logic_vector(31 downto 0);
--signal which controls the mux that chooses what data to write into
--the registers
signal MEM_MemToReg			: std_logic;
--write control signal for registers
signal MEM_reg_write_control	: std_logic;
--register address to write
signal MEM_reg_write_addr	: std_logic_vector(4 downto 0);
--enables branch condition
signal MEM_branch			: std_logic_vector(1 downto 0);
--forwarded data
signal MEM_fwrd				: std_logic_vector(31 downto 0);

-- WB STAGE SIGNALS
--data read from data memory
signal WB_mem_data			: std_logic_vector(31 downto 0);
--ALU result
signal WB_ALUresult			: std_logic_vector(31 downto 0);
--signal which controls the mux that chooses what data to write into
--the registers
signal WB_MemToReg			: std_logic;
--data to write at registers
signal WB_reg_write_data	: std_logic_vector(31 downto 0);
--write control signal for registers
signal WB_reg_write_control	: std_logic;
--register address to write
signal WB_reg_write_addr	: std_logic_vector(4 downto 0);

begin
------------------------------------------------------------------------
-- IF STAGE
------------------------------------------------------------------------
PCCounter: process(Clk, reset, IF_flush)
begin
	--if reset is 1, the next instruction address will be the first
	--position in the memory
	if reset = '1' or IF_flush = '1' then
		IF_PC <= (others => '0');
	--obtain next instruction address on rising edge of clock
	elsif rising_edge(Clk) then
		IF_PC <= IF_next_PC;
	end if;
end process;

--PC + 4
IF_PC_4 <= std_logic_vector(unsigned(IF_PC) + to_unsigned(4,32));
--mux for next instruction address
next_PC: process(IF_PCSrc, IF_PC_4, EX_branch_addr)
begin
	case IF_PCSrc is
	when "00" =>
		--no branch
		IF_next_PC <= IF_PC_4;
	when "01" =>
		--preemptively branches
		IF_next_PC <= EX_branch_addr;
	when "10" =>
		--branch condition failed, resumes previous instruction path
		IF_next_PC <= ID_PC_4;
	when others =>
    	null;
	end case;
end process;

--obtain instruction from memory
I_Addr <= IF_PC;
--always reads from instruction memory
I_RdStb <= '1';
--never writes on instruction memory
I_WrStb <= '0';
--could be left alone, processor will never write into the instruction
--memory
I_DataOut <= (others => '0');

------------------------------------------------------------------------
-- IF/ID INTERMEDIATE REGISTERS
------------------------------------------------------------------------
IF_input: process(Clk, reset, ID_flush)
begin
	--if reset is 1, data is flushed
	if reset = '1' or ID_flush = '1' then
		ID_instruction <= (others => '0');
		ID_PC_4 <= (others => '0');
	--advance data on rising edge of clock
	elsif rising_edge(Clk) then
		ID_instruction <= I_DataIn;
		ID_PC_4 <= IF_PC_4;
	end if;
end process;

------------------------------------------------------------------------
-- REGISTERS INSTANCE
------------------------------------------------------------------------
Registers_inst: registers
port map(
	Clk 		=> Clk,
	reset 		=> reset,
	w_control	=> WB_reg_write_control,
	reg1_addr	=> ID_instruction(25 downto 21),
	reg2_addr	=> ID_instruction(20 downto 16),
	reg_write	=> WB_reg_write_addr,
	data_write	=> WB_reg_write_data,
	data1_read	=> ID_data1_read,
	data2_read	=> ID_data2_read
);

------------------------------------------------------------------------
-- CONTROL UNIT
------------------------------------------------------------------------
CUnit: process(ID_instruction)
begin
	case ID_instruction(31 downto 26) is
	-- R-type
	when "000000" =>
		ID_reg_write_control <= '0' when ID_instruction(5 downto 0) = "000000" else '1';
		ID_MemToReg <= '0';
		ID_branch <= "00";
		ID_mem_read <= '0';
		ID_mem_write <= '0';
		ID_RegDst <= '1';
		ID_ALUop <= "10";
		ID_ALUsrc <= '0';
	-- LW
	when "100011" =>
		ID_reg_write_control <= '1';
		ID_MemToReg <= '1';
		ID_branch <= "00";
		ID_mem_read <= '1';
		ID_mem_write <= '0';
		ID_RegDst <= '0';
		ID_ALUop <= "01";
		ID_ALUsrc <= '1';
	-- SW
	when "101011" =>
		ID_reg_write_control <= '0';
		ID_MemToReg <= '1';
		ID_branch <= "00";
		ID_mem_read <= '0';
		ID_mem_write <= '1';
		ID_RegDst <= '0';
		ID_ALUop <= "01";
		ID_ALUsrc <= '1';
	-- BEQ
	when "000100" =>
		ID_reg_write_control <= '0';
		ID_MemToReg <= '0';
		ID_branch <= "10";
		ID_mem_read <= '0';
		ID_mem_write <= '0';
		ID_RegDst <= '0';
		ID_ALUop <= "11";
		ID_ALUsrc <= '0';
	-- LUI
	when "001111" =>
		ID_reg_write_control <= '1';
		ID_MemToReg <= '0';
		ID_branch <= "00";
		ID_mem_read <= '0';
		ID_mem_write <= '0';
		ID_RegDst <= '0';
		ID_ALUop <= "00";
		ID_ALUsrc <= '1';
	-- BNE
	when "000101" =>
		ID_reg_write_control <= '0';
		ID_MemToReg <= '0';
		ID_branch <= "01";
		ID_mem_read <= '0';
		ID_mem_write <= '0';
		ID_RegDst <= '0';
		ID_ALUop <= "11";
		ID_ALUsrc <= '0';
	-- others, inserts a NOP
	when others =>
		ID_reg_write_control <= '0';
		ID_MemToReg <= '0';
		ID_branch <= "00";
		ID_mem_read <= '0';
		ID_mem_write <= '0';
		ID_RegDst <= '0';
		ID_ALUop <= "00";
		ID_ALUsrc <= '0';
	end case;
end process;

------------------------------------------------------------------------
-- ID STAGE
------------------------------------------------------------------------
--extend immediate to 32 bits
ID_immediate <= x"FFFF"&ID_instruction(15 downto 0) 
				when ID_instruction(15) = '1'
				else x"0000"&ID_instruction(15 downto 0);

--rs address
ID_rs <= ID_instruction(25 downto 21);
--rt address
ID_rt <= ID_instruction(20 downto 16);
--rd address
ID_rd <= ID_instruction(15 downto 11);

------------------------------------------------------------------------
-- ID/EX INTERMEDIATE REGISTERS
------------------------------------------------------------------------
ID_input: process(Clk, reset, EX_flush)
begin
	--if reset is 1, data is flushed
	if reset = '1' or EX_flush = '1' then
		EX_reg_write_control <= '0';
		EX_MemToReg <= '0';
		EX_branch <= "00";
		EX_mem_read <= '0';
		EX_mem_write <= '0';
		EX_rs <= (others => '0');
		EX_rt <= (others => '0');
		EX_rd <= (others => '0');
		EX_immediate <= (others => '0');
		EX_data1_read <= (others => '0');
		EX_data2_read <= (others => '0');
		EX_ALUop <= (others => '0');
		EX_RegDst <= '0';
		EX_ALUSrc <= '0';
		EX_PC_4 <= (others => '0');
	--advance data on rising edge of clock
	elsif rising_edge(Clk) then
		EX_reg_write_control <= ID_reg_write_control;
		EX_MemToReg <= ID_MemToReg;
		EX_branch <= ID_branch;
		EX_mem_read <= ID_mem_read;
		EX_mem_write <= ID_mem_write;
		EX_rs <= ID_rs;
		EX_rt <= ID_rt;
		EX_rd <= ID_rd;
		EX_immediate <= ID_immediate;
		EX_data1_read <= ID_data1_read;
		EX_data2_read <= ID_data2_read;
		EX_ALUop <= ID_ALUop;
		EX_RegDst <= ID_RegDst;
		EX_ALUSrc <= ID_ALUSrc;
		EX_PC_4 <= ID_PC_4;
	end if;
end process;

------------------------------------------------------------------------
-- ALU INSTANCE
------------------------------------------------------------------------
ALU_inst: ALU
port map(
	input_a	=> EX_data1_read,
	input_b	=> EX_ALU_B_input,
	ALUctrl	=> EX_ALUctrl,
	zero	=> EX_zero,
	result	=> EX_ALUresult
);

------------------------------------------------------------------------
-- ALU CONTROL
------------------------------------------------------------------------
ALU_control: process(EX_ALUop, EX_immediate)
begin
	case EX_ALUop is
	when "10" =>
		-- ADD operation
		if EX_immediate(5 downto 0) = "100000" then
			EX_ALUctrl <= "010";
		-- SUB operation
		elsif EX_immediate(5 downto 0) = "100010" then
			EX_ALUctrl <= "110";
		-- AND operation
		elsif EX_immediate(5 downto 0) = "100100" then
			EX_ALUctrl <= "000";
		-- OR operation
		elsif EX_immediate(5 downto 0) = "100101" then
			EX_ALUctrl <= "001";
		-- Less Than
		elsif EX_immediate(5 downto 0) = "101010" then
			EX_ALUctrl <= "111";
		end if;
	when "01" =>
		-- add base with offset
		EX_ALUctrl <= "010";
	when "11" =>
		-- subtract for comparison
		EX_ALUctrl <= "110";
	when "00" =>
		-- move left 16 positions (multiply by 65536)
		EX_ALUctrl <= "100";
	when others =>
		-- does nothing
		EX_ALUctrl <= "011";
	end case;
end process;

------------------------------------------------------------------------
-- FORWARDING UNIT
------------------------------------------------------------------------
RAW_manager: process(EX_reg_write_control, EX_rs, EX_rt, MEM_reg_write_addr, WB_reg_write_addr)
begin
	if MEM_reg_write_control = '1' and EX_rs = MEM_reg_write_addr then
		--next instruction wants to read from register rs before it's updated
		EX_ALUfwrdA <= "01";
	elsif WB_reg_write_control = '1' and EX_rs = WB_reg_write_addr then
		--second next instruction wants to read from register rs before it's updated
		EX_ALUfwrdA <= "10";
	else EX_ALUfwrdA <= "00";
	end if;

	if MEM_reg_write_control = '1' and EX_rt = MEM_reg_write_addr then
		--next instruction wants to read from register rt before it's updated
		EX_ALUfwrdB <= "01";
	elsif WB_reg_write_control = '1' and EX_rt = WB_reg_write_addr then
		--second next instruction wants to read from register rt before it's updated
		EX_ALUfwrdB <= "10";
	else EX_ALUfwrdB <= "00";
	end if;
end process;

------------------------------------------------------------------------
-- EX STAGE
------------------------------------------------------------------------
ALU_A_mux: process(EX_data1_read, EX_ALUfwrdA, MEM_fwrd, WB_reg_write_data)
begin
	case EX_ALUfwrdA is

	--no forwarding required
	when "00" =>
		EX_ALU_A_input <= EX_data1_read;
	--RAW risk on consecutive instructions
	when "01" =>
		EX_ALU_A_input <= MEM_fwrd;
	--RAW risk on instructions at distance 2
	when "10" =>
		EX_ALU_A_input <= WB_reg_write_data;
	when others =>
		EX_ALU_A_input <= (others => '0');
	end case;
end process;

ALU_B_mux: process(EX_data2_read, EX_ALUfwrdB, MEM_fwrd, WB_reg_write_data)
begin
	case EX_ALUfwrdB is

	--no forwarding required
	when "00" =>
		EX_p_ALU_b_input <= EX_data2_read;
	--RAW risk on consecutive instructions
	when "01" =>
		EX_p_ALU_b_input <= MEM_fwrd;
	--RAW risk on instructions at distance 2
	when "10" =>
		EX_p_ALU_b_input <= WB_reg_write_data;
	when others =>
		EX_p_ALU_b_input <= (others => '0');
	end case;
end process;

--mux which chooses the ALU's b input
EX_ALU_B_input <= EX_p_ALU_B_input when EX_ALUSrc = '0'
					else EX_immediate;

--mux which choooses the write address for the registers
EX_reg_write_addr <= EX_rt when EX_RegDst = '0'
						else EX_rd;

--jump address calculation
EX_branch_addr <= std_logic_vector(unsigned(EX_immediate(29 downto 0) & "00")+unsigned(EX_PC_4));

--effective branch prediction
PCSrc: process(EX_branch, IF_flush, EX_PC_4)
begin
	if IF_flush = '1' then
    	--a branch condition failed
    	IF_PCSrc <= "10";
	elsif EX_branch(1) = '1' or EX_branch(0) = '1' then
    	--preemptively branches
    	IF_PCSrc <= "01";
	else
    	--no branch to be considered
    	IF_PCSrc <= "00";
	end if;
end process;

------------------------------------------------------------------------
-- EX/MEM INTERMEDIATE REGISTERS
------------------------------------------------------------------------
EX_input: process(Clk, reset, EX_flush)
begin
	if reset = '1' or EX_flush = '1' then
		MEM_ALUresult 			<= (others => '0');
		MEM_MemToReg 			<= '0';
		MEM_branch 				<= "00";
		MEM_data2_read 			<= (others => '0');
		MEM_zero				<= '0';
		MEM_reg_write_addr		<= (others => '0');
		MEM_reg_write_control	<= '0';
		MEM_mem_write			<= '0';
		MEM_mem_read			<= '0';
	elsif rising_edge(Clk) then
		MEM_ALUresult 			<= EX_ALUresult;
		MEM_MemToReg 			<= EX_MemToReg;
		MEM_branch 				<= EX_branch;
		MEM_data2_read 			<= EX_p_ALU_B_input;
		MEM_zero				<= EX_zero;
		MEM_reg_write_addr		<= EX_reg_write_addr;
		MEM_reg_write_control	<= EX_reg_write_control;
		MEM_mem_write			<= EX_mem_write;
		MEM_mem_read			<= EX_mem_read;
	end if;
end process;

------------------------------------------------------------------------
-- MEM STAGE
------------------------------------------------------------------------
--target address in data memory
D_Addr <= MEM_ALUresult;
--data to write into the data memory
D_DataOut <= MEM_data2_read;
--reading control signal for data memory
D_RdStb <= MEM_mem_read;
--writing control signal for data memory
D_WrStb <= MEM_mem_write;
--data received from data memory
MEM_mem_data <= D_DataIn;

--forwarded data mux, source of RAW risk may be a LOAD instruction
--instead of an arithmetic/logic operation
MEM_fwrd <= MEM_ALUresult when MEM_mem_read = '0' else MEM_mem_data;

------------------------------------------------------------------------
-- CONTROL HAZARD MANAGEMENT
------------------------------------------------------------------------
--Predicts effective jump
HAZARD_unit: process(MEM_branch, MEM_zero)
begin
	if (MEM_branch(0) = '1' and MEM_zero = '0') or (MEM_branch(1) = '1' and MEM_zero = '1') then
		--branch condition was effective
		IF_flush <= '0';
		ID_flush <= '1';
		EX_flush <= '1';
	elsif MEM_branch(0) = '1' or MEM_branch(1) = '1' then
		--branch condition failed
		IF_flush <= '1';
		ID_flush <= '0';
		EX_flush <= '0';
	else
		--no branch was made
		IF_flush <= '0';
		ID_flush <= '0';
		EX_flush <= '0';
	end if;
end process;

------------------------------------------------------------------------
-- MEM/WB INTERMEDIATE REGISTERS
------------------------------------------------------------------------
MEM_input: process(Clk, reset)
begin
	if reset = '1' then
		WB_ALUresult		<= (others => '0');
		WB_MemToReg			<= '0';
		WB_mem_data			<= (others => '0');
		WB_reg_write_addr	<= (others => '0');
		WB_reg_write_control<= '0';
	elsif rising_edge(Clk) then
		WB_ALUresult		<= MEM_ALUresult;
		WB_MemToReg			<= MEM_MemToReg;
		WB_mem_data			<= MEM_mem_data;
		WB_reg_write_addr	<= MEM_reg_write_addr;
		WB_reg_write_control<= MEM_reg_write_control;
	end if;
end process;

------------------------------------------------------------------------
-- WB STAGE
------------------------------------------------------------------------
--mux that chooses data to write in registers
WB_reg_write_data <= WB_mem_data when WB_MemToReg = '1' else WB_ALUresult;
end processor_arq;
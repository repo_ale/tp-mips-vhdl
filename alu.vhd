library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ALU is Port (
	input_a	: in std_logic_vector (31 downto 0);
	input_b	: in std_logic_vector (31 downto 0);
	ALUctrl	: in std_logic_vector (2 downto 0);
	zero	: out std_logic;
	result	: out std_logic_vector (31 downto 0));
end ALU;

architecture Behavioral of ALU is

signal aux : std_logic_vector (31 downto 0);

begin

	process(input_a, input_b, ALUctrl)
	begin
	case ALUctrl is
		when "000" => aux <= input_a and input_b;
		when "001" => aux <= input_a or input_b;
		when "010" => aux <= std_logic_vector(signed(input_a) + signed(input_b));
		when "110" => aux <= std_logic_vector(signed(input_a) - signed(input_b));
		when "111" => 
			if input_a < input_b then
		aux <= x"00000001";
		else aux <= x"00000000";
		end if;
		when "100" => aux <= input_b(15 downto 0) & x"0000";  
		when others => aux <= x"00000000";   
	end case;
	end process;

	result <= aux;

	process(aux)
	begin
		if aux = x"00000000" then
			zero <= '1';
		else
			zero <= '0';
		end if;
	end process;

end Behavioral;

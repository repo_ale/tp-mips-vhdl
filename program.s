.data 
num0: .word 1 # posic 0
num1: .word 2 # posic 4
num2: .word 4 # posic 8 
num3: .word 8 # posic 12 
num4: .word 16 # posic 16 
num5: .word 32 # posic 20
num6: .word 0 # posic 24
num7: .word 0 # posic 28
num8: .word 0 # posic 32
num9: .word 0 # posic 36
num10: .word 0 # posic 40
num11: .word 0 # posic 44
.text 
main:
  lw $t1, 0($zero)	#t1=1
  lw $t2, 4($zero)	#t2=2
  lw $t3, 8($zero)	#t3=4
  lw $t4, 12($zero)	#t4=8
  lw $t5, 16($zero)	#t5=10
  lw $t6, 20($zero)	#t6=20
  sw $t1, 24($zero)
  sw $t2, 28($zero)
  sw $t3, 32($zero)
  sw $t4, 36($zero)
  sw $t5, 40($zero)
  sw $t6, 44($zero)
  lw $t1, 24($zero)	#t1=1
  lw $t2, 28($zero)	#t2=2
  lw $t3, 32($zero) #t3=4
  lw $t4, 36($zero) #t4=8
  lw $t5, 40($zero) #t5=10
  lw $t6, 44($zero) #t6=20
  add $t7, $t1, $t2	#t7=t1+t2=3
  add $s0, $t3, $t4	#s0=t3+t4=4+8=12
  sub $s1, $t5, $t1	#s1=t5-t1=10-1=9
  sub $s2, $t6, $t2	#s2=t6-t2=20-2=18
  and $s3, $t1, $t2	#s3=t1 and t2=0..01 and 0..10=0..0=0
  and $s4, $t7, $t2	#s4=t7 and t2=0..11 and 0..10=0..10=2
  
  or $s5, $t1, $t2	#s5=t1 or t2=0..01 or 0..10=0..011=3
  or $s6, $s0, $t2	#s6=s0 or t2=0..1100 or 0..0010=0..1110=14
  slt $s7, $t1, $t2	#s7=t1 < t2=1<2=1
  slt $t8, $s0, $t2	#t8=s0 < t2=12<2=0
  beq $t1, $s7, L1	#t1 equal s7=1 equal 1=True
  add $s7,$s7,$s7	#es ejecutado en parte y luego descartado
  add $s6,$s6,$s6	#es ejecutado en parte y luego descartado
  add $s4,$s4,$s4	#no es ejecutado
  
  L1: bne $s4, $s5, L2	#s4 notequal s5=2 notequal 3=True
  add $s7,$s7,$s7	#es ejecutado en parte y luego descartado
  add $s6,$s6,$s6	#es ejecutado en parte y luego descartado
  add $s5,$s5,$s5	#no es ejecutado
  add $s4,$s4,$s4	#no es ejecutado
  
  L2:   lui $t1, 1	#t1=1 << 16=65536
  lui $t2, 2		#t2=10 << 16=196608
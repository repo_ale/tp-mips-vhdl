# TP-MIPS-VHDL

>> Alumnos : Batyk Alexis - Fernández de la Torre Alejo - Maldonado Christian - Mondin Leandro

El objetivo de este práctico es implementar un microprocesador MIPS en VHDL. En concreto, se va a realizar la versión segmentada del microprocesador. El resultado debe ser un micro capaz de realizar en el caso ideal una instrucción por ciclo de reloj.

No se pide que el microprocesador soporte todo el juego de instrucciones completo, sino las siguientes instrucciones: add, sub, and, or, lw, sw, slt, lui ,beq y bne.
Todos los registros deben reiniciarse asíncronamente y funcionar por flanco de subida del reloj.

El procesador implementado por este grupo incluye manejo de riesgos de control y RAW. Además, se utilizó la librería estándar de VHDl, numeric_std, para manejar operaciones aritméticas en lugar de las provistas por la cátedra.

***La implementación descrita en negrita no está reflejada en los gráficos, pues éstos no incluyen manejo de riesgos ni la instrucción BNE.***

![Micro completo](https://gitlab.com/repo_ale/tp-mips-vhdl/-/raw/master/Imagenes/Fotocompleta.png)

# Etapa 1: IF
>Instruction Fetching (Búsqueda de Instrucción)

![Etapa1-IF](https://gitlab.com/repo_ale/tp-mips-vhdl/-/raw/master/Imagenes/Etapa1.png)

Luego de ocurrir un flanco alto de reloj, el PC entregará la dirección de la próxima instrucción a la memoria de instrucciones (la cual se encuentra separada de la memoria de datos), quien emitirá como salida la próxima instrucción.

Por otro lado, dicha dirección será la entrada de un sumador, cuya salida es la suma de esa misma dirección más cuatro. Esta es la potencial (debido a que puede haber un salto) dirección de la siguiente instrucción. Se suma de a cuatro de acuerdo a la implementación de la Memoria de instrucciones: cada instrucción ocupa cuatro bytes y la memoria provee acceso a cada byte individual.

Luego, la próxima dirección potencial (dirección del PC + 4) es filtrada por un mux en conjunto con una entrada asincrónica y en base a un selector, PCSrc, que también es una señal asincrónica. La otra entrada es una dirección de salto, y el selector es una señal generada en la etapa EX frente a una instrucción de salto. Si el selector está desactivado, el mux elige la dirección calculada por el sumador. Si está activado, quiere decir que se está ejecutando un salto de manera preventiva, y el mux elige la dirección de salto. **Para implementar una estrategia de predicción de saltos efectivos, el multiplexor que elige la dirección de la siguiente instrucción fue extendido a tres entradas, y PCSrc a dos bits. La tercer entrada es la dirección de la última instrucción ejecutada antes de un salto que no resultó efectivo, más cuatro. Esta entrada es elegida cuando PCSrc="10".**

La dirección del PC + 4 y la salida de la memoria de instrucciones pasan a los registros intermedios IF/ID.

# Etapa 2: ID
>Instruction Decoding (Decodificación de Instrucción)

![Etapa2-ID](https://gitlab.com/repo_ale/tp-mips-vhdl/-/raw/master/Imagenes/Etapa2.png)

Al producirse un flanco ascendente, los registros de sincronización IF/ID entregan las señales a ser procesadas a la etapa ID. La próxima instrucción (la salida del sumador) será requerida por otra etapa en caso de que haya un salto. La instrucción actual (la salida de la memoria de instrucciones) es separada en varios campos.

Esta etapa se encarga de decodificar la entrada, considerando todas las posibles entradas instrucciones. Se la puede dividir en tres partes que funcionan en simultáneamente:

1. La Unidad de Control se encarga de emitir las señales de control para definir la Ruta de datos (Data Path) de la instrucción, es decir, decide qué instrucción se está ejecutando.
2. El Banco de Registros del procesador es la memoria temporal del procesador. Rs y Rt, campos de la instrucción actual, son direcciones de resgistros; se conectan a Read Register 1 y 2 respectivamente, entradas de lectura del banco de registros. Aunque no siempre se requiere leer dos registros a la vez, el banco siempre entrega las lecturas de los registros en rs y rt a través de las salidas Read Register 1 y Read Register 2, las cuales son almacenadas en los registros de sincronización ID/EX. Para instrucciones de memoria, base ocupa el lugar de rs. La escritura se maneja en la etapa WB.
3. A su vez, se realiza una extensión del campo offset/immediate (Instrucción[15-0]), de 16 a 32 bits.


En 1. se decide la operación a realizar, mientras que en 2. y 3. se obtiene los operandos.

Las salidas de la Unidad de Control, la dirección de la instrucción más 4, las salidas del Banco de Registros, el offset/immediate extendido y las direcciones **Rs (Instrucción[25-21])**, Rt (Instrucción[20-16]) y Rd (Instrucción[15-11]) son almacenados en los registros de sincronización ID/EX.

# Etapa 3: EX
>Execution (ejecución)

![Etapa3-EX](https://gitlab.com/repo_ale/tp-mips-vhdl/-/raw/master/Imagenes/Etapa3.png)

Al producirse un flanco en subida del reloj, los registros de sincronización ID/EX actualizarán las señales de entrada de la etapa EX. Algunas señales de control, para WB y MEM, irán directo a los registros intermedios EX/MEM para las etapas siguientes (“next step”).

El offset/immediate extendido de la etapa anterior es multiplicado por cuatro (con el doble shift a la izquierda) y sumado a la dirección de la próxima instrucción (calculada en IF). Se debe recordar que la multiplicación por cuatro es requerida por la implementación de la memoria de instrucciones: el offset indica cuántas instrucciones se deben saltar, pero cada instrucción ocupa cuatros posiciones de la memoria. El resultado es una dirección de salto **que será enviada directamente a al multiplexor en la etapa IF para realizar un salto preventivo. Este salto solo se realizará si la señal de control branch es "10" o "01", es decir, se está ejecutando una instrucción de salto.**

Por su parte, la señal de control de dos bits ALUOp indica a la unidad de control del ALU si el campo Func de la instrucción (Instrucción[5-0]) debe ser considerado para elegir la operación a realizar (ADD, SUB, AND, OR Y SLT; ALUOp = "10"), si solo se debe sumar para calcular una dirección de memoria (LW y SW; ALUOp="01"), si solo se debe restar para una comparación de equivalencia (BEQ y BNE; "11"), o si solo se debe realizar un shift de 16 bits (LUI; ALUOp="00"). La ALU realizará entonces la operación comandada por ALU Control en base a los parámetros de entrada: los datos almacenados en los registros rs y rt, o bien, el dato en Rs y el immediate extendido. Esta decisión es tomada por un mux gobernado por ALUSrc, y depende de los operando que requiera la operación: ADD, SUB, AND, OR, SLT, BEQ y BNE utilizan operandos obtenidos del banco de registros, mientras que LW, SW y LUI utilizan un operando obtenido del banco y un dato immediato. El resultado de la ALU es almacenado en los registros intermedios EX/MEM.

Todas las instrucciones que escriben en el banco de registros utilizan el campo Rd para definir la dirección de escritura, a excepción de las instrucciones SW y LUI que utilizan el campo Rt. El selector RegDst decide cuál campo utilizar. El campo elegido es almacenado en los registros intermedios EX/MEM.

**Si está habilitada la señal de escritura de banco de registros en las etapas MEM o WB (todavía no escribieron, pero van a escribir), la Unidad de Adelantamiento de Datos compara las direcciones Rs y Rt con las direcciones de escritura en MEM o WB para determinar si se debe adelantar datos antes de que sean escritos en un registro. Esta unidad emite dos señales de selección para dos multiplexores en la etapa EX, los cuales eligen de dónde se obtiene los operandos de la ALU (uno de los operandos puede ser un immediate, en cuyo caso el selector no afecta la ejecución de la instrucción): si una instrucción debe leer de un registro que antes debe ser modificado por una instrucción anterior, y éstas son consecutivas, el selector correspondiente al registro de lectura tomará el valor "01" y se adelantará un dato de la etapa MEM; si la instrucción que escribe es LW, el dato será leído de la memoria de datos, en caso contrario, el dato será el resultado obtenido de la ALU en el ciclo anterior. Si las mismas instrucciones están a una distancia 2, el selector correspondiente tomará el valor "10" y se adelantará el dato a escribir de la etapa WB.**

# Etapa 4: MEM
>Memory access (acceso a memoria)

![Etapa4-MEM](https://gitlab.com/repo_ale/tp-mips-vhdl/-/raw/master/Imagenes/Etapa4.png)

Al producirse un flanco en subida del reloj, los registros de sincronización EX/MEM actualizarán las señales de entrada de la etapa MEM. Las señales de control WB se postergarán hacia la siguiente etapa (next step), yendo directamente a los registros de sincronización MEM/WB.

Dentro de las señales de control de la etapa actual se tiene la señal branch. Para que un salto definido por una instrucción BEQ sea efectivo, se deben cumplir dos condiciones: que se desee hacer un salto **(Branch="10")** y que la resta entre los datos de los registros Rs y Rt sea cero, o sea que los datos sean iguales. **Análogamente, para que un salto definido por una instrucción BNE sea efectivo, se deben cumplir dos condiciones: que se desee hacer un salto (Branch="01") y que la resta entre los datos de los registros Rs y Rt sea distinto de cero, o sea que los datos sean diferentes. El resultado de estas operaciones son tres señales de vaciado (flushing) para los registros intermedios IF/ID e ID/EX, y para el PC. Si el saltó resulto efectivo, los registros IF/ID e ID/EX son vaciados; si el salto no resultó efectivo, se vacía la instrucción adelantada en el ciclo anterior, y se vuelve al camino de instrucciones anterior (la instrucción siguiente a la que se está ejecutando en la etapa ID). En el gráfico, la compuerta AND sería reemplazada con dos compuertas AND con sus salidas conectadas a una compuerta OR ([branch(1)=1 and zero=1] or [branch(0)=1 and zero=0]) y otra compuerta OR (branch(1)=1 or branch(0)=1), cuyas salidas estarían conectadas a una unidad de manejo de riesgos de control con tres salidas, las señales de vaciado (IF_flush, ID_flush, EX_flush).** *La señal branch tiene 3 estados útiles, el cuarto estado que no se empleó, podría haber sido utilizado para implementar un salto no condicional.*

Por su parte, la Memoria de Datos es controlada por las señales MemRead y MemWrite, y el resultado obtenido del ALU será la entra Address de la memoria. Si la instrucción que se está ejecutando es LW, sólo la señal MemRead estará activada, y se emitirá como salida de la memoria el contenido la dirección Addess a través de Read data.  Si la instrucción que se está ejecutando es SW, sólo la señal MemWrite estará activada, esto producirá que en la dirección del campo Address se grabe el contenido de Write data. Este último es el dato en el registro Rt, y fue provisto por la etapa anterior.

Por último, la dirección calculada por la ALU, aparte de entrar como Address a la memoria, también es almacenada en los registros de sincronización EX/MEM para la siguiente etapa, al igual que la dirección elegida por el mux controlado por RegDst en la etapa anterior.

# Etapa 5: WB
>Write back (post escritura)

![Etapa5-WB](https://gitlab.com/repo_ale/tp-mips-vhdl/-/raw/master/Imagenes/Etapa5.png)

El objetivo de esta etapa es actualizar el Banco de Registros de la etapa ID. La señal de control RegWrite estará activa en caso de que se deba hacer la escritura.

Write register es la dirección del registro a guardar, la cual puede ser Rd si es una instrucción Tipo-R, o Rt si es un LW o LUI (se debe recordar que esto se decidió en la etapa de EX con el mux controlado por RegDst).

Finalmente, MemToReg decidirá qué es lo que se guardará en Write data. Si está activado, se trata de una instrucción LW y los datos serán los extraídos de la Memoria de Datos en la etapa anterior. Si no lo está, es una Tipo-R y el dato será  el resultado procesado por la ALU que se viene trayendo desde la etapa de EX.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Registers is Port (
	Clk 		: in std_logic;
	reset 		: in std_logic;
	w_control	: in std_logic;
	reg1_addr	: in std_logic_vector (4 downto 0);
	reg2_addr	: in std_logic_vector (4 downto 0);
	reg_write	: in std_logic_vector (4 downto 0);
	data_write	: in std_logic_vector (31 downto 0);
	data1_read	: out std_logic_vector (31 downto 0);
	data2_read	: out std_logic_vector (31 downto 0));
end Registers;

architecture Behavioral of Registers is
	type ram_type is array (31 downto 0) of std_logic_vector (31 downto 0);
	signal regs : ram_type;
begin

	process (Clk, reset)
	begin
		if reset = '1' then
			regs <= (others => x"00000000");
		elsif falling_edge(Clk) then
			if (w_control = '1') then
				regs(to_integer(unsigned(reg_write))) <= data_write;
			end if;
		end if;
	end process;

	data1_read <= regs(to_integer(unsigned(reg1_addr))) when reg1_addr /= "00000" else
				(others => '0');

	data2_read <= regs(to_integer(unsigned(reg2_addr))) when reg2_addr /= "00000" else
				(others => '0');				

end Behavioral;